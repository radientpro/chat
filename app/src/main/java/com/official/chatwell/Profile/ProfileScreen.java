package com.official.chatwell.Profile;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.Toast;

import com.official.chatwell.DashBoard.DashboardActivity;
import com.official.chatwell.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileScreen extends AppCompatActivity implements ProfileContract.View{


    private ProfilePresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_screen);
        ButterKnife.bind(this);
        mPresenter = new ProfilePresenter(this);
    }

    @OnClick(R.id.submit)
    void onSubmit(){
        Toast.makeText(this, "Yet to Implement", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.skip)
    void onSkip(){
        openDashboard();
    }

    @Override
    public void openDashboard() {
        startActivity(new Intent(this, DashboardActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }
}
