package com.official.chatwell;

import android.content.Context;
import android.content.SharedPreferences;

import retrofit2.Retrofit;

/**
 * Created by USER on 23-07-2017.
 */

public class Constants {
    public static final String BASE_URL = "http://thedesigningworld.co/chatfox/android/apis/";
    public static final String USER_ID = "id";
    public static final String USER_NAME = "name";
    public static final String USER_PHONE = "phone";
    public static final String USER_AUTH = "password";
    public static final String USER_STATUS = "user_status";
    public static final String CREATED = "created";
    public static final String MODIFIED = "modified";
    public static final String OTP = "otp";
    public static final String VERIFIED = "verified";

    public static Retrofit getWebClient(){
        return  new Retrofit.Builder().baseUrl(BASE_URL).build();
    }

    public static SharedPreferences getUserPreferences(Context context) {
        return context.getSharedPreferences("session", Context.MODE_PRIVATE);
    }
}
