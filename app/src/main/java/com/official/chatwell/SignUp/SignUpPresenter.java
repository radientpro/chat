package com.official.chatwell.SignUp;

import com.official.chatwell.Constants;
import com.official.chatwell.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by USER on 23-07-2017.
 */

class SignUpPresenter {
    private SignUpContract.View view;

    public SignUpPresenter(SignUpContract.View view) {
        this.view = view;
    }

    public void onSignUpClickListener() {

        String uname = view.getUserName();
        if (uname.isEmpty()) {
            view.setUserNameError(R.string.uname_error);
            return;
        }

        String password = view.getPassword();
        if (password.isEmpty()) {
            view.setPasswordError(R.string.pwd_error);
            return;
        }

        String c_password = view.getConfirmPassword();
        if (c_password.isEmpty()) {
            view.setConfirmPasswordError(R.string.c_pwd_error);
            return;
        }

        if (!password.equals(c_password)) {
            view.passwordNotMatched(R.string.ped_not_matched);
            return;
        }

        String phone = view.getPhoneNumber();
        if (phone.isEmpty()) {
            view.setPhoneError(R.string.phone_error);
            return;
        }

        signUpWebServiceCall(uname, password, phone);
    }

    private void signUpWebServiceCall(String uname, String password, String phone) {
        view.showLoader();
        String action = "registration";
        SignUpContract.Service service = Constants.getWebClient().create(SignUpContract.Service.class);
        Call<ResponseBody> call = service.signUpMethod(action, uname, password, phone);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                view.hideLoader();
                if (response.isSuccessful()) {
                    try {
                        String res = response.body().string();
                        JSONObject object = new JSONObject(res);
                        int status = object.optInt("status");
                        if (status == 1) {
                            view.parseAndSaveResponse(object.optJSONObject("userInfo"));
                            view.registerSuccess();
                        }else {
                            view.showMessage(object.optString( "msg"));
                        }

                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                view.hideLoader();
                view.showNetworkError(R.string.internet_error);
            }
        });
    }

}
