package com.official.chatwell.SignUp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.StringRes;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.official.chatwell.Constants;
import com.official.chatwell.DashBoard.DashboardActivity;
import com.official.chatwell.R;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends AppCompatActivity implements SignUpContract.View {

    @BindView(R.id.username)
    TextInputLayout mUsername;
    @BindView(R.id.password)
    TextInputLayout mPassword;
    @BindView(R.id.c_password)
    TextInputLayout c_Password;
    @BindView(R.id.mno)
    TextInputLayout mPhone;
    @BindView(R.id.buttonSignUp)
    Button mSignUp;
    @BindView(R.id.progressBar)
    ProgressBar mPregress;
    private SignUpPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        mPresenter = new SignUpPresenter(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Sign Up");
    }

    @OnClick(R.id.buttonSignUp)
    void onSignUpClicked() {
        mPresenter.onSignUpClickListener();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public String getUserName() {
        return mUsername.getEditText().getText().toString().trim();
    }

    @Override
    public void setUserNameError(@StringRes int resId) {
        mUsername.setError(getString(resId));
    }

    @Override
    public String getPassword() {
        return mPassword.getEditText().getText().toString().trim();
    }

    @Override
    public void setPasswordError(@StringRes int resId) {
        mPassword.setError(getString(resId));
    }

    @Override
    public String getConfirmPassword() {
        return c_Password.getEditText().getText().toString().trim();
    }

    @Override
    public void setConfirmPasswordError(@StringRes int resId) {
        c_Password.setError(getString(resId));
    }

    @Override
    public void passwordNotMatched(@StringRes int resId) {
        c_Password.setError(getString(resId));
    }

    @Override
    public String getPhoneNumber() {
        return mPhone.getEditText().getText().toString().trim();
    }

    @Override
    public void setPhoneError(@StringRes int resId) {
        mPhone.setError(getString(resId));
    }

    @Override
    public void showLoader() {
        mSignUp.setVisibility(View.INVISIBLE);
        mPregress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        mSignUp.setVisibility(View.VISIBLE);
        mPregress.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showNetworkError(@StringRes int resId) {
        Toast.makeText(this, getString(resId), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void parseAndSaveResponse(JSONObject userInfo) {
        SharedPreferences.Editor editor  = Constants.getUserPreferences(this).edit();
        editor.putString(Constants.USER_ID, userInfo.optString("id"));
        editor.putString(Constants.USER_NAME, userInfo.optString("name"));
        editor.putString(Constants.USER_PHONE, userInfo.optString("phone"));
        editor.putString(Constants.USER_AUTH, userInfo.optString("password"));
        editor.putString(Constants.USER_STATUS, userInfo.optString("user_status"));
        editor.putString(Constants.CREATED, userInfo.optString("created"));
        editor.putString(Constants.MODIFIED, userInfo.optString("modified"));
        editor.apply();
    }

    @Override
    public void registerSuccess() {
        Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(this, DashboardActivity.class));
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
}
