package com.official.chatwell.SignUp;

import android.support.annotation.StringRes;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by USER on 23-07-2017.
 */

class SignUpContract {
    public interface View {
        String getUserName();

        void setUserNameError(@StringRes int resId);

        String getPassword();

        void setPasswordError(@StringRes int resId);

        String getConfirmPassword();

        void setConfirmPasswordError(@StringRes int resId);

        void passwordNotMatched(@StringRes int resId);

        String getPhoneNumber();

        void setPhoneError(@StringRes int resId);

        void showLoader();

        void showNetworkError(@StringRes int resId);

        void hideLoader();

        void parseAndSaveResponse(JSONObject userInfo);

        void registerSuccess();

        void showMessage(String msg);
    }

    public interface Service {
        @FormUrlEncoded
        @POST("api.php")
        Call<ResponseBody> signUpMethod(@Field("action") String action, @Field("name") String uname, @Field("password") String password, @Field("phone") String phone);
    }
}
