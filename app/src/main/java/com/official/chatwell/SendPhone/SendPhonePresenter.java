package com.official.chatwell.SendPhone;

import com.official.chatwell.Constants;
import com.official.chatwell.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by USER on 30-07-2017.
 */

class SendPhonePresenter {
    private SendPhoneContract.View view;

    public SendPhonePresenter(SendPhoneContract.View view) {

        this.view = view;
    }

    public void setOnNextClicked() {
        String phone = view.getPhoneNumber();
       // if (phone.isEmpty() || phone.length() < 10){
       //     view.showPhoneError(R.string.enter_number_error);
       //     return;
       // }

       // sendPhoneWebServiceCall(phone);
        view.showOTPVerifyScreen();
    }

    private void sendPhoneWebServiceCall(String phone) {
        view.disableNextButton(R.string.wait);
        String action = "registration";
        SendPhoneContract.Service service = Constants.getWebClient().create(SendPhoneContract.Service.class);
        Call<ResponseBody> call = service.sendPhoneMethod(action, phone);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                view.enableNextButton(R.string.next);
                if (response.isSuccessful()){
                    try {
                        String res = response.body().string();
                        JSONObject object = new JSONObject(res);
                        int status = object.optInt("status");
                        switch (status) {
                            case 1:
                            view.saveResponse(object.optJSONObject("userInfo"));
                            view.showOTPVerifyScreen();
                                break;
                            case 2:
                                  view.showMessage(object.optString("msg"));
                                view.showOTPVerifyScreen();
                                break;
                            case 0:
                                  view.showMessage(object.optString("msg"));
                                view.showOTPVerifyScreen();
                                break;
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                view.enableNextButton(R.string.next);
                view.showNetworkError(R.string.internet_error);
            }
        });
    }

    public void checkAlreadyLoggedIn() {
        String auth = view.getPreferencesValue(Constants.USER_AUTH);
        if (auth.length() > 1){
            view.showDashboard();
        }
    }
}
