package com.official.chatwell.SendPhone;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.official.chatwell.Constants;
import com.official.chatwell.DashBoard.DashboardActivity;
import com.official.chatwell.R;
import com.official.chatwell.VerifyOTP.VerifyOTPScreen;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SendPhoneActivity extends AppCompatActivity implements SendPhoneContract.View {

    @BindView(R.id.phoneNumber)
    EditText mPhone;
    @BindView(R.id.next)
    Button mNext;
    private SendPhonePresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_phone);
        ButterKnife.bind(this);
        mPresenter = new SendPhonePresenter(this);
        mPresenter.checkAlreadyLoggedIn();
     //   showDashboard();
    }

    @OnClick(R.id.next)
    void onNext() {
        mPresenter.setOnNextClicked();
    }

    @Override
    public String getPhoneNumber() {
        return mPhone.getText().toString().trim();
    }

    @Override
    public void showPhoneError(@StringRes int resId) {
        mPhone.setError(getString(resId));
    }

    @Override
    public void showOTPVerifyScreen() {
        startActivity(new Intent(this, VerifyOTPScreen.class));
    }

    @Override
    public void disableNextButton(@StringRes int resId) {
        mNext.setEnabled(false);
        mNext.setText(getString(resId));
    }

    @Override
    public void enableNextButton(@StringRes int resId) {
        mNext.setEnabled(true);
        mNext.setText(getString(resId));
    }

    @Override
    public void showNetworkError(int resId) {
        Toast.makeText(this, getString(resId), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void saveResponse(JSONObject userInfo) {
        SharedPreferences.Editor editor = Constants.getUserPreferences(this).edit();
        editor.putString(Constants.USER_ID, userInfo.optString("id"));
        editor.putString(Constants.USER_NAME, userInfo.optString("name"));
        editor.putString(Constants.USER_PHONE, userInfo.optString("phone"));
        editor.putString(Constants.USER_AUTH, userInfo.optString("password"));
        editor.putString(Constants.USER_STATUS, userInfo.optString("user_status"));
        editor.putString(Constants.CREATED, userInfo.optString("created"));
        editor.putString(Constants.MODIFIED, userInfo.optString("modified"));
        editor.putString(Constants.VERIFIED, userInfo.optString("verified"));
        editor.putString(Constants.OTP, userInfo.optString("register_otp"));
        editor.apply();
    }

    @Override
    public String getPreferencesValue(String key) {
        return Constants.getUserPreferences(this).getString(key, "");
    }

    @Override
    public void showDashboard() {
        startActivity(new Intent(this, DashboardActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }
}
