package com.official.chatwell.SendPhone;

import android.support.annotation.StringRes;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

import static android.R.attr.action;

/**
 * Created by USER on 30-07-2017.
 */

class SendPhoneContract {
    public interface View {
        String getPhoneNumber();

        void showPhoneError(@StringRes int resId);

        void showOTPVerifyScreen();

        void disableNextButton(@StringRes int resId);

        void enableNextButton(@StringRes int resId);

        void showNetworkError(int resId);

        void showMessage(String msg);

        void saveResponse(JSONObject userInfo);

        String getPreferencesValue(String key);

        void showDashboard();
    }

    public interface Service{
        @FormUrlEncoded
        @POST("api.php")
        Call<ResponseBody> sendPhoneMethod(@Field("action") String action, @Field("phone") String phone);
    }
}
