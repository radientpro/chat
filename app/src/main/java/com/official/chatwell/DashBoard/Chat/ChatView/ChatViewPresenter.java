package com.official.chatwell.DashBoard.Chat.ChatView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by USER on 03-09-2017.
 */

class ChatViewPresenter {
    private ChatViewContract.View view;

    public ChatViewPresenter(ChatViewContract.View view) {
        this.view = view;
    }

    public void setChatData() {
        view.setAdapter(getDummyData());
    }

    private List<ChatModel> getDummyData() {
        List<ChatModel> data = new ArrayList<>();
        for (int i=0; i<5; i++){
            ChatModel model = new ChatModel();
           /* if (i==4){
                model.setMessage("This is the long long long long long long long ......... sample Message "+(i+1));
            }else {*/
                model.setMessage("This is the sample Message "+(i+1));
           // }
            model.setTime((i+1)+" hours ago");
            data.add(model);
        }
        return data;
    }
}
