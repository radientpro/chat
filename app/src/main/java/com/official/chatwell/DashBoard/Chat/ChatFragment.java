package com.official.chatwell.DashBoard.Chat;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.official.chatwell.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatFragment extends Fragment implements ChatContract.View{
    private ChatPresenter mPresenter;
    @BindView(R.id.mList)
    RecyclerView mList;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_chat, container, false);
        ButterKnife.bind(this, v);
        mPresenter = new ChatPresenter(this);
        mPresenter.prepareRecyclerData();
        return v;
    }

    @Override
    public void setAdapter(List<ModelRecycler> staticData) {
        mList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mList.setAdapter(new ChatAdapter(getActivity(), staticData));
    }
}
