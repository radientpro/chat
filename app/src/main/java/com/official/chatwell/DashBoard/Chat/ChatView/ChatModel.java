package com.official.chatwell.DashBoard.Chat.ChatView;

/**
 * Created by USER on 03-09-2017.
 */

class ChatModel {
    private String message;
    private String time;
    private String proPic;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getProPic() {
        return proPic;
    }

    public void setProPic(String proPic) {
        this.proPic = proPic;
    }
}
