package com.official.chatwell.DashBoard.Chat.ChatView;

import java.util.List;

/**
 * Created by USER on 03-09-2017.
 */

class ChatViewContract {
    public interface View {
        void setAdapter(List<ChatModel> mData);
    }
}
