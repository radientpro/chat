package com.official.chatwell.DashBoard;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.official.chatwell.Constants;
import com.official.chatwell.DashBoard.Calls.CallsFragment;
import com.official.chatwell.DashBoard.Chat.ChatFragment;
import com.official.chatwell.DashBoard.Contacts.ContactsFragment;
import com.official.chatwell.Login.LoginActivity;
import com.official.chatwell.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DashboardActivity extends AppCompatActivity implements DashboardContract.View, TabLayout.OnTabSelectedListener {

    private DashboardPresenter mPresenter;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);
        mPresenter = new DashboardPresenter(this);
        tabLayout.addOnTabSelectedListener(this);
        selectDefaultTab();
    }

    private void selectDefaultTab() {
        inflateFragment(1);
        TabLayout.Tab tab = tabLayout.getTabAt(1);
        assert tab != null;
        tab.select();
    }

/*
    @OnClick(R.id.buttonLogout)
    void onLogoutClicked(){
      //  mPresenter.onLogoutClicked();
    }
*/

    @Override
    public String getValueFromPreferences(String key) {
        return Constants.getUserPreferences(this).getString(key, "4");
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void openLoginScreen() {
        startActivity(new Intent(this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        inflateFragment(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }

    private void inflateFragment(int position){
        Fragment fragment;
        if (position==0){
            fragment = new CallsFragment();
        }else if (position==1){
            fragment = new ChatFragment();
        }else {
            fragment = new ContactsFragment();
        }
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.commit();
    }
}
