package com.official.chatwell.DashBoard.Chat.ChatView;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;

import com.official.chatwell.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ChatActivity extends AppCompatActivity implements ChatViewContract.View{

    private ChatViewPresenter mPresenter;
    @BindView(R.id.chatList)
    RecyclerView mChaList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        mPresenter = new ChatViewPresenter(this);
        mPresenter.setChatData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==android.R.id.home)
            finish();
        return true;
    }

    @Override
    public void setAdapter(List<ChatModel> mData) {
        LinearLayoutManager manager = new LinearLayoutManager(this);
        manager.setStackFromEnd(true);
        mChaList.setLayoutManager(manager);
        mChaList.setAdapter(new ChatViewAdapter(ChatActivity.this, mData));
    }
}
