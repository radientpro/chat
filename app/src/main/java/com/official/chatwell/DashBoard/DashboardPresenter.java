package com.official.chatwell.DashBoard;

import com.official.chatwell.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by USER on 23-07-2017.
 */

class DashboardPresenter {
    private DashboardContract.View view;

    public DashboardPresenter(DashboardContract.View view) {

        this.view = view;
    }

    public void onLogoutClicked() {
        String action = "logout";
        //String action = "logoutOtherDevices";
        String userId = view.getValueFromPreferences(Constants.USER_ID);
        DashboardContract.Service service = Constants.getWebClient().create(DashboardContract.Service.class);
        Call<ResponseBody> call = service.logoutMethod(action, userId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    try {
                        String res = response.body().string();
                        JSONObject object = new JSONObject(res);
                        int Status = object.optInt("status");
                        if (Status == 1){
                            view.showMessage(object.optString("msg"));
                            view.openLoginScreen();
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }


}
