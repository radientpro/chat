package com.official.chatwell.DashBoard.Chat;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.official.chatwell.DashBoard.Chat.ChatView.ChatActivity;
import com.official.chatwell.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by USER on 28-07-2017.
 */

class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    private final Context context;
    private final List<ModelRecycler> mData;
    private final LayoutInflater inflater;

    public ChatAdapter(Context context, List<ModelRecycler> mData) {
        this.context = context;
        this.mData = mData;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(inflater.inflate(R.layout.chat_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ModelRecycler model = mData.get(position);
        holder.mName.setText(model.name);
        holder.mTime.setText(model.time);
        holder.mMsg.setText(model.message);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView mName;
       @BindView(R.id.time)
        TextView mTime;
       @BindView(R.id.message)
        TextView mMsg;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    context.startActivity(new Intent(context, ChatActivity.class));
                }
            });
        }

    }
}
