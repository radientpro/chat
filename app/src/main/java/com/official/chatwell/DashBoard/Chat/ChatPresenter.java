package com.official.chatwell.DashBoard.Chat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by USER on 03-09-2017.
 */

class ChatPresenter {
    private ChatContract.View view;

    public ChatPresenter(ChatContract.View view) {

        this.view = view;
    }

    public void prepareRecyclerData() {
        view.setAdapter(getStaticData());
    }

    private List<ModelRecycler> getStaticData() {
        List<ModelRecycler> data = new ArrayList<>();
        for (int i = 1; i <= 15; i++) {
            ModelRecycler model = new ModelRecycler();
            model.name = "Name " + i;
            model.message = "This is the last message received " + i;
            model.time = "12:40 PM";
            data.add(model);
        }
        return data;
    }
}
