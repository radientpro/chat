package com.official.chatwell.DashBoard;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by USER on 23-07-2017.
 */

class DashboardContract {
    public interface View {
        String getValueFromPreferences(String key);

        void showMessage(String msg);

        void openLoginScreen();
   }

    public interface Service {
        @FormUrlEncoded
        @POST("api.php")
        Call<ResponseBody> logoutMethod(@Field("action") String action,@Field("userId") String userId);
    }
}
