package com.official.chatwell.Login;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.official.chatwell.Constants;
import com.official.chatwell.DashBoard.DashboardActivity;
import com.official.chatwell.R;
import com.official.chatwell.SignUp.SignUpActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements LoginContract.View {

    @BindView(R.id.username)
    TextInputLayout mUsername;
    @BindView(R.id.password)
    TextInputLayout mPassword;
    @BindView(R.id.signUp)
    TextView mSignUp;
    private LoginPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        mPresenter = new LoginPresenter(this);
        showDashboard();
    }

    @OnClick(R.id.btnLogin)
    void loginClick() {
        mPresenter.onLoginClicked();
    }

    @OnClick(R.id.signUp)
    void signUpClick() {
        mPresenter.onSignUpClicked();
    }


    @Override
    public String getUsername() {
        return mUsername.getEditText().getText().toString().trim();
    }

    @Override
    public void showUsernameError(int resId) {
        mUsername.setError(getString(resId));
    }

    @Override
    public String getPassword() {
        return mPassword.getEditText().getText().toString().trim();
    }

    @Override
    public void showPasswordError(int resId) {
        mPassword.setError(getString(resId));
    }

    @Override
    public void showDashboard() {
       startActivity(new Intent(this, DashboardActivity.class));
    }

    @Override
    public void showSignUpScreen() {
        startActivity(new Intent(this, SignUpActivity.class));
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void saveResponse(String userId) {
        SharedPreferences.Editor editor = Constants.getUserPreferences(this).edit();
        editor.putString(Constants.USER_ID, userId);
        editor.apply();
    }
}
