package com.official.chatwell.Login;

import android.support.annotation.StringRes;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by USER on 16-07-2017.
 */

class LoginContract {
    public interface View {
        String getUsername();

        void showUsernameError(@StringRes int resId);

        String getPassword();

        void showPasswordError(@StringRes int resId);

        void showDashboard();

        void showSignUpScreen();

        void showMessage(String msg);

        void saveResponse(String userId);
    }

    public interface Service {
        @FormUrlEncoded
        @POST("api.php")
        Call<ResponseBody> loginMethod(@Field("action") String action,@Field("phone") String uname,@Field("password") String pass);
    }
}
