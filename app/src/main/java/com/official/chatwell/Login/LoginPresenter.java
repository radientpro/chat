package com.official.chatwell.Login;

import com.official.chatwell.Constants;
import com.official.chatwell.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by USER on 16-07-2017.
 */

class LoginPresenter {
    private LoginContract.View view;

    public LoginPresenter(LoginContract.View view) {

        this.view =  view;
    }

    public void onLoginClicked() {
        String uname = view.getUsername();
        if (uname.isEmpty()){
            view.showUsernameError(R.string.username_error);
            return;
        }

        String pass = view.getPassword();
        if (pass.isEmpty()){
            view.showPasswordError(R.string.pass_error);
            return;
        }

        view.showDashboard();
       // loginWebServiceCall(uname, pass);
    }

    private void loginWebServiceCall(String uname, String pass) {
        String action = "login";
        LoginContract.Service service = Constants.getWebClient().create(LoginContract.Service.class);
        Call<ResponseBody> call = service.loginMethod(action, uname, pass);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    try {
                        String res = response.body().string();
                        JSONObject object = new JSONObject(res);
                        int status = object.optInt("status");
                        if (status == 1){
                            view.saveResponse(object.optString("userId"));
                            view.showDashboard();
                        }else if (status ==2){
                            view.showMessage(object.optString("msg"));
                        }else if (status == 0){
                            view.showMessage(object.optString("msg"));
                        }
                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    public void onSignUpClicked() {
        view.showSignUpScreen();
    }
}
