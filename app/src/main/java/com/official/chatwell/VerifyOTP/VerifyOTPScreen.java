package com.official.chatwell.VerifyOTP;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.official.chatwell.Constants;
import com.official.chatwell.Profile.ProfileScreen;
import com.official.chatwell.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VerifyOTPScreen extends AppCompatActivity implements OTPContract.View {

    @BindView(R.id.otp)
    EditText mOtp;
    @BindView(R.id.submit)
    Button mSubmit;
    private OTPPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otpscreen);
        ButterKnife.bind(this);
        mPresenter = new OTPPresenter(this);
    }


    @OnClick(R.id.submit)
    void onSubmit() {
        mPresenter.onSubmitClick();
    }

    @Override
    public String getOTP() {
        return mOtp.getText().toString().trim();
    }

    @Override
    public void showOTPError(@StringRes int resId) {
        mOtp.setError(getString(resId));
    }

    @Override
    public void disableSubmit(@StringRes int resId) {
        mSubmit.setText(getString(resId));
        mSubmit.setEnabled(false);
    }

    @Override
    public String getPreferencesValue(String key) {
        return Constants.getUserPreferences(this).getString(key, "");
    }

    @Override
    public void enabledSubmit(@StringRes int resId) {
        mSubmit.setText(getString(resId));
        mSubmit.setEnabled(true);
    }

    @Override
    public void showMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showNetworkError(@StringRes int resId) {
        Toast.makeText(this, getString(resId), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void openProfileScreen() {
        startActivity(new Intent(this, ProfileScreen.class));
    }

    @Override
    public void saveAuthCode(String authCode) {
        SharedPreferences.Editor editor = Constants.getUserPreferences(this).edit();
        editor.putString(Constants.USER_AUTH, authCode);
        editor.apply();
    }
}
