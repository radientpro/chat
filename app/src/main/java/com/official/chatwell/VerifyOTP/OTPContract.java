package com.official.chatwell.VerifyOTP;

import android.support.annotation.StringRes;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by USER on 30-07-2017.
 */

class OTPContract {
    public interface View {
        String getOTP();

        void showOTPError(@StringRes int resId);

        void disableSubmit(@StringRes int resId);

        String getPreferencesValue(String key);

        void enabledSubmit(@StringRes int resId);

        void showMessage(String msg);

        void showNetworkError(@StringRes int resId);

        void openProfileScreen();

        void saveAuthCode(String authCode);
    }

    public interface Service {
        @FormUrlEncoded
        @POST("api.php")
        Call<ResponseBody> otpMethod(@Field("action") String action, @Field("otp") String otp, @Field("userId") String userId);
    }
}
