package com.official.chatwell.VerifyOTP;

import com.official.chatwell.Constants;
import com.official.chatwell.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by USER on 30-07-2017.
 */

class OTPPresenter {
    private OTPContract.View view;

    public OTPPresenter(OTPContract.View view) {

        this.view = view;
    }

    public void onSubmitClick() {
        String otp =view.getOTP();
//        if (otp.isEmpty()){
//            view.showOTPError(R.string.enter_otp);
//            return;
//        }

        otpWebservice(otp);
    }

    private void otpWebservice(String otp) {
        String action = "verify_otp";
        String userId = view.getPreferencesValue(Constants.USER_ID);
        view.disableSubmit(R.string.verifying);
        OTPContract.Service service = Constants.getWebClient().create(OTPContract.Service.class);
        Call<ResponseBody> call = service.otpMethod(action, otp, userId);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                view.enabledSubmit(R.string.submit);
                if (response.isSuccessful()){
                    try {
                        String res = response.body().string();
                        JSONObject object = new JSONObject(res);
                        int status = object.optInt("status");
                        if (status == 1) {
                            view.saveAuthCode(object.optString("authCode"));
                            view.openProfileScreen();
                        }else {
                            view.showMessage(object.optString( "msg"));
                        }

                    } catch (IOException | JSONException e) {
                        e.printStackTrace();
                    }
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                view.enabledSubmit(R.string.submit);
                view.showNetworkError(R.string.internet_error);
            }
        });
    }
}
